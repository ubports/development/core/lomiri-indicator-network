
include_directories(
    "${CMAKE_SOURCE_DIR}/src"
    "${CMAKE_SOURCE_DIR}/src/indicator"
    "${CMAKE_SOURCE_DIR}/src/qdbus-stubs"
    "${CMAKE_BINARY_DIR}/src/qdbus-stubs"
    ${CMAKE_CURRENT_BINARY_DIR}
)

###########################
# Secret agent mini binary
###########################

add_definitions(
    -DSECRET_AGENT_BIN="${CMAKE_CURRENT_BINARY_DIR}/secret-agent-test-bin"
)

add_executable(
    secret-agent-test-bin
    secret-agent/secret-agent-main.cpp
)

target_link_libraries(
    secret-agent-test-bin
    Qt5::Core
)

target_link_libraries(
    secret-agent-test-bin
    agent-static
)

###################
# Unit test binary
###################

set(
    UNIT_TESTS_SRC

    indicator/menuitems/test-access-point-item.cpp
    indicator/menuitems/test-switch-item.cpp

    menumodel-cpp/test-menu-exporter.cpp

    secret-agent/test-secret-agent.cpp
)

set_source_files_properties(
    "${DATA_DIR}/nm-secret-agent.xml"
    PROPERTIES
    INCLUDE "dbus-types.h"
)

qt5_add_dbus_interface(
    UNIT_TESTS_SRC
    "${DATA_DIR}/nm-secret-agent.xml"
    SecretAgentInterface
)

add_executable(
    unit-tests
    ${UNIT_TESTS_SRC}
)

target_link_libraries(
    unit-tests
    Qt5::Core
    Qt5::DBus
    Qt5::Test
)

target_link_libraries(
    unit-tests
    test-utils
    lomiri-indicator-network-service-static
    ${TEST_DEPENDENCIES_LDFLAGS}
    ${GLIB_LDFLAGS}
    ${GTEST_LIBRARIES}
    ${GMOCK_LIBRARIES}
)

add_test(
    unit-tests
    unit-tests
)
